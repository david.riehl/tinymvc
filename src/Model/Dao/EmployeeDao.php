<?php
namespace TinyMVC\Model\Dao;

use Exception;
use PDO;

class EmployeeDao
{
    private static $classname = "TinyMVC\\Model\\Employee";
    private static $ctorargs = ["id", "firstname", "lastname", "birthdate", "id_agency"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT *
                  FROM `employee`;";

        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 
            self::$classname, 
            self::$ctorargs);
        $items = $sth->fetchAll();

        $dao->close();
        return $items;
    }

    public static function get($id)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT *
                  FROM `employee`
                  WHERE `id` = :id
                  ;";
                  
        $sth = $dbh->prepare($query);

        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 
            self::$classname, 
            self::$ctorargs);
        $item = $sth->fetch();

        $dao->close();
        return $item;
    }

    public static function delete($id)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "DELETE FROM `employee`
                  WHERE `id` = :id;
                 ";
                  
        $sth = $dbh->prepare($query);

        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $dao->close();
    }

    public static function update($id, $firstname, $lastname, $birthdate, $id_agency)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "UPDATE `employee`
                  SET
                    `firstname` = :firstname,
                    `lastname`  = :lastname,
                    `birthdate` = :birthdate,
                    `id_agency` = :id_agency
                  WHERE `id` = :id;
                 ";
                  
        $sth = $dbh->prepare($query);

        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":firstname", $firstname, PDO::PARAM_STR, 30);
        $sth->bindParam(":lastname", $lastname, PDO::PARAM_STR, 30);
        $sth->bindParam(":birthdate", $birthdate, PDO::PARAM_STR, 10);
        $sth->bindParam(":id_agency", $id_agency, PDO::PARAM_INT);

        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $dao->close();
    }
}