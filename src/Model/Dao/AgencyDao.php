<?php
namespace TinyMVC\Model\Dao;

use Exception;
use PDO;

class AgencyDao
{
    private static $classname = "TinyMVC\\Model\\Agency";
    private static $ctorargs = ["id", "name", "address", "id_city"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT *
                  FROM `agency`;";

        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 
            self::$classname, 
            self::$ctorargs);
        $items = $sth->fetchAll();

        $dao->close();
        return $items;
    }

    public static function get($id)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT *
                  FROM `agency`
                  WHERE `id` = :id
                  ;";
                  
        $sth = $dbh->prepare($query);

        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if (! $result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 
            self::$classname, 
            self::$ctorargs);
        $item = $sth->fetch();

        $dao->close();
        return $item;
    }
}