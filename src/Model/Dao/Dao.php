<?php
namespace TinyMVC\Model\Dao;

use PDO;

class Dao
{
    private $driver = "mysql";
    private $host = "127.0.0.1";
    private $port = "3306";
    private $dbname = "entreprise";
    private $charset = "utf8";
    private $user = "entreprise";
    private $password = "dnHgDkVRGJJlUko1";
    private $dsn;
    private $dbh;
    public function get_dbh() { return $this->dbh; }

    public function __construct()
    {
        $this->dsn = 
            "{$this->driver}:".
            "host={$this->host};".
            "port={$this->port};".
            "dbname={$this->dbname};".
            "charset={$this->charset};";
    }

    public function open()
    {
        $this->dbh = new PDO($this->dsn, $this->user, $this->password);
    }

    public function close()
    {
        unset($this->dbh);
    }
}