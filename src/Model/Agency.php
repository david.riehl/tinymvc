<?php

namespace TinyMVC\Model;

use TinyMVC\Model\Dao\AgencyDao;

class Agency extends BusinessObject
{
    private $id;
    public function get_id() { return $this->id; }
    public function set_id($value) { $this->id = $value; }

    private $name;
    public function get_name() { return $this->name; }
    public function set_name($value) { $this->name = $value; }

    private $address;
    public function get_address() { return $this->address; }
    public function set_address($value) { $this->address = $value; }

    private $id_city;
    public function get_id_city() { return $this->id_city; }
    public function set_id_city($value) { $this->id_city = $value; }

    private $city;
    public function get_city()
    {
        if ($this->city == null)
        {
            $this->city = City::get($this->id_city);
        }
        return $this->city;
    }
    public function set_city($value)
    {
        $this->city = $value;
        $this->id_city = $this->city->id;
    }

    protected function get_object_vars() : array
    {
        return get_object_vars($this);
    }

    public static function get_all()
    {
        return AgencyDao::get_all();
    }

    public static function get($id)
    {
        return AgencyDao::get($id);
    }
}