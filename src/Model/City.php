<?php

namespace TinyMVC\Model;

use TinyMVC\Model\Dao\CityDao;

class City extends BusinessObject
{
    private $id;
    public function get_id() { return $this->id; }
    public function set_id($value) { $this->id = $value; }

    private $name;
    public function get_name() { return $this->name; }
    public function set_name($value) { $this->name = $value; }

    private $zip_code;
    public function get_zip_code() { return $this->zip_code; }
    public function set_zip_code($value) { $this->zip_code = $value; }

    protected function get_object_vars() : array
    {
        return get_object_vars($this);
    }

    public static function get_all()
    {
        return CityDao::get_all();
    }

    public static function get($id)
    {
        return CityDao::get($id);
    }
}