<?php

namespace TinyMVC\Model;

use TinyMVC\Model\Dao\EmployeeDao;

class Employee extends BusinessObject
{
    private $id;
    public function get_id() { return $this->id; }
    public function set_id($value) { $this->id = $value; }

    private $firstname;
    public function get_firstname() { return $this->firstname; }
    public function set_firstname($value) { $this->firstname = $value; }

    private $lastname;
    public function get_lastname() { return $this->lastname; }
    public function set_lastname($value) { $this->lastname = $value; }

    private $birthdate;
    public function get_birthdate() { return $this->birthdate; }
    public function set_birthdate($value) { $this->birthdate = $value; }

    private $id_agency;
    public function get_id_agency() { return $this->id_agency; }
    public function set_id_agency($value) { $this->id_agency = $value; }

    private $agency;
    public function get_agency()
    {
        if ($this->agency == null)
        {
            $this->agency = Agency::get($this->id_agency);
        }
        return $this->agency;
    }
    public function set_agency($value)
    {
        $this->agency = $value;
        $this->id_agency = $this->agency->id;
    }

    protected function get_object_vars() : array
    {
        return get_object_vars($this);
    }

    public static function get_all()
    {
        return EmployeeDao::get_all();
    }

    public static function get($id)
    {
        return EmployeeDao::get($id);
    }

    public static function delete($id)
    {
        return EmployeeDao::delete($id);
    }

    public static function update($id, $firstname, $lastname, $birthdate, $id_agency)
    {
        return EmployeeDao::update($id, $firstname, $lastname, $birthdate, $id_agency);
    }
}