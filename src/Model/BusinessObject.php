<?php

namespace TinyMVC\Model;

use Exception;
use InvalidArgumentException;
use JsonSerializable;

abstract class BusinessObject implements JsonSerializable
{
    public function __construct()
    {
        $num_args = func_num_args();

        if ($num_args) {

            // on récupère les propriétés de l'objet
            $object_vars = $this->get_object_vars();
            $properties = array_keys($object_vars);
            $num_props = count($properties);

            // on vérifique la correspondance entre 
            // le nombre de propriétés et le nombre
            // de paramètres
            if ($num_args > $num_props) {
                throw new InvalidArgumentException();
            }

            // on récupère la liste des paramètres
            $args = func_get_args();

            // on associe tous les paramètres aux propriétés
            foreach($args as $index => $value)
            {
                $prop = $properties[$index];
                $this->$prop = $value;
            }
        }
    }

    public function __get($property)
    {
        $class = get_class($this);

        if (! property_exists($this, $property)) {
            throw new Exception("Property $property does not exist in $class.");
        }

        $method = "get_$property";
        if (! method_exists($this, $method)) {
            throw new Exception("Unauthorized get access on $property from $class.");
        }

        return $this->$method();
    }

    public function __set($property, $value) {
        $class = get_class($this);

        if (! property_exists($this, $property)) {
            throw new Exception("Property $property does not exist in $class.");
        }

        $method = "set_$property";
        if (! method_exists($this, $method)) {
            throw new Exception("Unauthorized set access on $property from $class.");
        }

        return $this->$method($value);
    }

    abstract protected function get_object_vars() : array;

    public function jsonSerialize()
    {
        return $this->get_object_vars();
    }
}