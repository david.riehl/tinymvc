    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= $base_path; ?>/vendor/components/jquery/jquery.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="<?= $base_path; ?>/vendor/popper/1.15.0/popper.min.js" integrity="sha384-Ogj/KJdOPqgL2rugK83q0gLBa756965bSkB050W1QxgmaRxIQdQlxYG4N3+D7NQR" crossorigin="anonymous"></script>
    <script src="<?= $base_path; ?>/vendor/twbs/bootstrap/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
