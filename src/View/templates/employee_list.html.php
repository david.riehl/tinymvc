<?php require_once "html_header.html.php"; ?>
<?php require_once "bootstrap_navbar.html.php"; ?>
        <main>
            <div class="container">
                <h1>Employees</h1>
                <form method="post">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Birth Date</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
        <?php foreach($employees as $employee): ?>
                            <tr>
                                <th scope="row"><?= $employee->id ?></td>
                                <td><?= $employee->firstname ?></td>
                                <td><?= $employee->lastname ?></td>
                                <td><?= $employee->birthdate ?></td>
                                <th>
                                    <button type="submit" class="btn btn-outline-info" formaction="<?= $base_path ?>/employee/<?= $employee->id ?>">Edit</button>
                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modalDelete" onclick="displayDeleteModal(<?= str_replace('"', "'", json_encode($employee)); ?>);">Delete</button>
                                </th>
                            </tr>
        <?php endforeach; ?>                    
                        </tbody>
                    </table>
                </form>
            </div>
        </main>
<?php require_once "bootstrap_js.html.php"; ?>
        <script>
            function displayDeleteModal(employee) 
            {
                $("#modalBody").html(
                    "Are you sure you want to delete "
                    + employee.firstname + " " + employee.lastname + "&nbsp;?"
                );
                $("#modalSubmitButton").attr(
                    "formaction",
                    "<?= $base_path; ?>/employee/delete/<?= $employee->id; ?>"
                );
            }
        </script>
<?php require_once "bootstrap_modal_delete.html.php"; ?>        
<?php require_once "html_footer.html.php"; ?>