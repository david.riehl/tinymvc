<?php require_once "html_header.html.php"; ?>
<?php require_once "bootstrap_navbar.html.php"; ?>
        <main>
            <div class="container">
                <h1>Agencies</h1>
                <form method="post">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">City</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
        <?php foreach($agencies as $agency): ?>
                            <tr>
                                <th scope="row"><?= $agency->id ?></td>
                                <td><?= $agency->name ?></td>
                                <td><?= $agency->city->name ?></td>
                                <th>
                                    <button type="submit" class="btn btn-outline-info" formaction="<?= $base_path ?>/agency/<?= $agency->id ?>">Edit</button>
                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modalDelete" onclick="displayDeleteModal(<?= str_replace('"', "'", json_encode($agency)); ?>);">Delete</button>
                                </th>
                            </tr>
        <?php endforeach; ?>                    
                        </tbody>
                    </table>
                </form>
            </div>
        </main>
<?php require_once "bootstrap_js.html.php"; ?>
        <script>
            function displayDeleteModal(agency) 
            {
                $("#modalBody").html(
                    "Are you sure you want to delete "
                    + agency.name + "&nbsp;?"
                );
                $("#modalSubmitButton").attr(
                    "formaction",
                    "<?= $base_path; ?>/agency/delete/<?= $agency->id; ?>"
                );
            }
        </script>
<?php require_once "bootstrap_modal_delete.html.php"; ?>        
<?php require_once "html_footer.html.php"; ?>