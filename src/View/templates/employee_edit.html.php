<?php require_once "html_header.html.php"; ?>
<?php require_once "bootstrap_navbar.html.php"; ?>
        <main>
            <div class="container">
                <h1>Edit Employee</h1>
                <form action="<?= $base_path ?>/employee/update/<?= $employee->id ?>" method="post">
                    <div class="form-group row">
                        <label for="firstname" 
                            class="col-sm-2 col-form-label">
                            firstname
                        </label>
                        <div class="col-sm-10">
                            <input type="text" 
                                class="form-control" 
                                id="firstname" name="firstname" 
                                value="<?= $employee->firstname ?>"
                                required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastname" 
                            class="col-sm-2 col-form-label">
                            lastname
                        </label>
                        <div class="col-sm-10">
                            <input type="text" 
                                class="form-control" 
                                id="lastname" name="lastname" 
                                value="<?= $employee->lastname ?>"
                                required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="birthdate" 
                            class="col-sm-2 col-form-label">
                            birthdate
                        </label>
                        <div class="col-sm-10">
                            <input type="date" 
                                class="form-control" 
                                id="birthdate" name="birthdate" 
                                value="<?= $employee->birthdate ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="agency" 
                            class="col-sm-2 col-form-label">
                            agency
                        </label>
                        <div class="col-sm-10">
                            <select id="agency" name="id_agency" class="form-control">
<?php foreach($agencies as $agency): ?>
                                <option 
                                    value="<?= $agency->id; ?>"
                                    <?= ($agency->id == $employee->id_agency)?"selected":""; ?>
                                ><?= $agency->name; ?></option>
<?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 text-right">
                            <button type="submit" 
                                class="btn btn-outline-primary"
                                name="btn-update">
                                Update
                            </button>
                            <button type="submit" 
                                class="btn btn-outline-secondary"
                                name="btn-cancel">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </main>
<?php require_once "bootstrap_js.html.php"; ?>
<?php require_once "html_footer.html.php"; ?>