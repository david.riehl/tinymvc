        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <a class="navbar-brand" href="#"><?= $title; ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item<?= (!isset($active) || $active == "Home")?" active":""; ?>">
                        <a class="nav-link" href="<?= $base_path; ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item<?= ($active == "Employees")?" active":""; ?>">
                        <a class="nav-link" href="<?= $base_path; ?>/employees">Employees</a>
                    </li>
                    <li class="nav-item<?= ($active == "Agencies")?" active":""; ?>">
                        <a class="nav-link" href="<?= $base_path; ?>/agencies">Agencies</a>
                    </li>
                </ul>
            </div>
        </nav>
