<?php require_once "html_header.html.php"; ?>
<?php require_once "bootstrap_navbar.html.php"; ?>
        <main>
            <div class="container">
                <h1>404 Error</h1>
                <p>Page not found</p>
            </div>
        </main>
<?php require_once "bootstrap_js.html.php"; ?>
<?php require_once "html_footer.html.php"; ?>