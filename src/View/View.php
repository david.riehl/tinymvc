<?php

namespace TinyMVC\View;

class View
{
    private static $template;
    private static $path = "src/View/templates/";
    private static $variables = [];

    public static function setTemplate($name)
    {
        self::$template = $name;
    }

    public static function bindVariable($name, $value)
    {
        // store value in a dictionnary
        self::$variables[$name] = $value;
    }

    public static function display()
    {
        require_once "template_internal_params.php";
        require_once "template_user_params.php";

        // generate local variables
        foreach(self::$variables as $name => $value)
        {
            $$name = $value;
        }

        // load template
        require self::$path . self::$template . ".html.php";
    }
}