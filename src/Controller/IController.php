<?php
namespace TinyMVC\Controller;

interface IController
{
    static function route();
}