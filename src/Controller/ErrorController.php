<?php

namespace TinyMVC\Controller;

use TinyMVC\View\View;
use TinyMVC\Model\Employee;

class ErrorController implements IController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/error/404", "ErrorController", "error404Action"));
        $router->addRoute(new Route("/{*}", "ErrorController", "error404Action"));
        $route = $router->findRoute();
        
        if (!$route) {
            $route = new Route("/{*}", "ErrorController", "error404Action");
        }
        $route->execute();
    }

    public static function error404Action()
    {
        View::setTemplate("404");
        View::display();
    }
}