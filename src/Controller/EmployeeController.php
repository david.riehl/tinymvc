<?php

namespace TinyMVC\Controller;

use TinyMVC\View\View;
use TinyMVC\Model\Employee;
use TinyMVC\Model\Agency;

class EmployeeController implements IController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/", "EmployeeController", "getAllAction"));
        $router->addRoute(new Route("/employees", "EmployeeController", "getAllAction"));
        $router->addRoute(new Route("/employee/delete/{id}", "EmployeeController", "deleteAction"));
        $router->addRoute(new Route("/employee/update/{id}", "EmployeeController", "updateAction"));
        $router->addRoute(new Route("/employee/{id}", "EmployeeController", "getAction"));
        $route = $router->findRoute();
        
        if (!$route) {
            $route = new Route("/{*}", "ErrorController", "error404Action");
        }
        $route->execute();
    }

    public static function getAllAction()
    {
        $employees = Employee::get_all();
        View::setTemplate("employee_list");
        View::bindVariable("active", "Employees");
        View::bindVariable("employees", $employees);
        View::display();
    }

    public static function getAction($id)
    {
        $employee = Employee::get($id);
        $agencies = Agency::get_all();
        View::setTemplate("employee_edit");
        View::bindVariable("active", "Employees");
        View::bindVariable("employee", $employee);
        View::bindVariable("agencies", $agencies);
        View::display();
    }

    public static function deleteAction($id)
    {
        Employee::delete($id);
        $router = new Router();
        $base_path = $router->getBasePath();
        header("location: $base_path/employees");
    }

    public static function updateAction($id)
    {
        if (isset($_POST["btn-update"]))
        {
            $firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_STRING);
            $birthdate = filter_input(INPUT_POST, "birthdate", FILTER_VALIDATE_REGEXP, ["options" => ["regexp" => "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/"]]);
            $id_agency = filter_input(INPUT_POST, "id_agency", FILTER_VALIDATE_INT);
            Employee::update($id, $firstname, $lastname, $birthdate, $id_agency);
        }
        $router = new Router();
        $base_path = $router->getBasePath();
        header("location: $base_path/employees");
    }
}