<?php

namespace TinyMVC\Controller;

use TinyMVC\View\View;
use TinyMVC\Model\Agency;
use TinyMVC\Model\City;

class AgencyController implements IController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/", "AgencyController", "getAllAction"));
        $router->addRoute(new Route("/agencies", "AgencyController", "getAllAction"));
        $router->addRoute(new Route("/agency/delete/{id}", "AgencyController", "deleteAction"));
        $router->addRoute(new Route("/agency/update/{id}", "AgencyController", "updateAction"));
        $router->addRoute(new Route("/agency/{id}", "AgencyController", "getAction"));
        $route = $router->findRoute();
        
        if (!$route) {
            $route = new Route("/{*}", "ErrorController", "error404Action");
        }
        $route->execute();
    }

    public static function getAllAction()
    {
        $agencies = Agency::get_all();
        View::setTemplate("Agency_list");
        View::bindVariable("active", "Agencies");
        View::bindVariable("agencies", $agencies);
        View::display();
    }

    public static function getAction($id)
    {
        $Agency = Agency::get($id);
        $agencies = Agency::get_all();
        View::setTemplate("Agency_edit");
        View::bindVariable("active", "Agencies");
        View::bindVariable("Agency", $Agency);
        View::bindVariable("agencies", $agencies);
        View::display();
    }

    public static function deleteAction($id)
    {
        $router = new Router();
        $base_path = $router->getBasePath();
        Agency::delete($id);
        header("location: $base_path/Agencies");
    }

    public static function updateAction($id)
    {
        if (isset($_POST["btn-update"]))
        {
            $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, "address", FILTER_SANITIZE_STRING);
            $id_city = filter_input(INPUT_POST, "id_city", FILTER_VALIDATE_INT);
            Agency::update($id, $name, $address, $id_city);
        }
        $router = new Router();
        $base_path = $router->getBasePath();
        header("location: $base_path/Agencies");
    }
}