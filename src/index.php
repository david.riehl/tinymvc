<?php
namespace TinyMVC;

use TinyMVC\Controller\Router;
use TinyMVC\controller\Route;

require "vendor/autoload.php";

$router = new Router();
$router->addRoute(new Route("/", "EmployeeController"));
$router->addRoute(new Route("/employees", "EmployeeController"));
$router->addRoute(new Route("/employee/{*}", "EmployeeController"));
$router->addRoute(new Route("/agencies", "AgencyController"));
$router->addRoute(new Route("/agency/{*}", "AgencyController"));
$route = $router->findRoute();

if (!$route) {
    $route = new Route("/{*}", "ErrorController", "error404Action");
}
$route->execute();
