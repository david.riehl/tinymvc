# Tiny MVC

PHP Tiny MVC Sample Project

- URL Routing (using Apache rewrite module)
- Model with Generic Dao and Business Object definition (forcing use of getter and setter methods)
- PHP Template management (with params attachment)
- Bootstrap Integration